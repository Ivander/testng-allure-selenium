# testNG-Allure-Selenium

UI testing https://jut.su/

API testing http://jsonplaceholder.typicode.com/

for quick start up:
1) cd into repo root
2) execute mvnw clean test site
3) execute mvnw io.qameta.allure:allure-maven:serve

Chrome driver is used for this testing

Requirements: JAVA_HOME variable must be added into system variable on your local machine.