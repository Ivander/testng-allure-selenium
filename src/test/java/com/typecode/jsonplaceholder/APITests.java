package com.typecode.jsonplaceholder;

import com.typecode.jsonplaceholder.Constants.APIEndpoints;
import com.typecode.jsonplaceholder.Constants.PostConstants;
import com.typecode.jsonplaceholder.Shapes.PostsShape;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class APITests {

    // Below you can see a few tests in BDD stile
    RequestSpecification requestSpec = new RequestSpecBuilder()
            .setBaseUri(APIEndpoints.baseUrl)
            .setContentType(ContentType.JSON)
            .setBasePath(APIEndpoints.posts)
            .build();

    @BeforeClass
    public void onStart() {
        RestAssured.filters(new AllureRestAssured());
    }

    @Test(groups = "BackEnd", priority = 1)
    public void createPost() {

        PostsShape postShape = new PostsShape(1, 101, PostConstants.TitleCreate, PostConstants.BodyCreate);

        given()
                .spec(requestSpec)
                .body(postShape)
                .when()
                .post()
                .then()
                .log().body()
                .assertThat()
                .body("id", equalTo(postShape.getId()));
    }

    @Test(groups = "BackEnd", priority = 4)
    public void deletePosts() {
        given()
                .spec(requestSpec)
                .when()
                .delete("/1")
                .then()
                .log().body()
                .assertThat()
                .statusCode(200);
    }

    @Test(groups = "BackEnd", priority = 2)
    public void getPosts() {

        given()
                .spec(requestSpec)
                .when()
                .get("/1")
                .then()
                .log()
                .body()
                .body(matchesJsonSchemaInClasspath("getPosts.json"));
    }

    @Test(groups = "BackEnd", priority = 3)
    public void updatePosts() {

        PostsShape postShape = new PostsShape(1, PostConstants.TitleUpdate, PostConstants.BodyUpdate);

        given()
                .spec(requestSpec)
                .body(postShape)
                .when()
                .post()
                .then()
                .log().body()
                .assertThat()
                .statusCode(201)
                .body(matchesJsonSchemaInClasspath("getPosts.json"));
    }
}