package com.typecode.jsonplaceholder.Constants;

public class APIEndpoints {

    public final static String baseUrl = "http://jsonplaceholder.typicode.com/";

    public final static String posts = "/posts";
}
