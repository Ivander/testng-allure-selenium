package com.typecode.jsonplaceholder.Constants;

public class PostConstants {
    public static final String TitleCreate = "title1";

    public static final String BodyCreate = "body1";

    public static final String TitleUpdate = "updated title";

    public static final String BodyUpdate = "updated body";
}
