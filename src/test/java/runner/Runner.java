package runner;

import com.typecode.jsonplaceholder.APITests;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import su.jut.UiTests;

public class Runner {

    public static void main(String[] args) {
        TestListenerAdapter tla = new TestListenerAdapter();
        TestNG testng = new TestNG();
        testng.setTestClasses(new Class[]{APITests.class, UiTests.class});
        testng.addListener(tla);
        testng.run();
    }
}
