package su.jut.PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LogInPage extends CommonPageObject {

    private final WebDriver driver;

    private static WebDriverWait wait;

    @FindBy(id = "login_panel")
    private WebElement logInFormContainer;

    @FindBy(id = "login_input1")
    private WebElement userNameField;

    @FindBy(id = "login_input2")
    private WebElement passwordField;

    @FindBy(id = "login_submit")
    private WebElement logInSubmitButton;

    @FindBy(linkText = "Напомнить пароль")
    private WebElement forgotPasswordLink;

    @FindBy(linkText = "Регистрация")
    private WebElement signInButton;

    @FindBy(id = "auth_via_vk")
    private WebElement authViaVkButton;

    public LogInPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @Override
    public void waitScreen() {
        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(logInFormContainer));
    }

    public String waitAndGetFailedLoginMessage(){
        wait = new WebDriverWait(driver, 10);
        WebElement messageDiv = driver.findElement(By.className("berrors"));
        wait.until(ExpectedConditions.visibilityOf(messageDiv));
        return messageDiv.getText();
    }

    /**
     * Set specified text into user name field.
     *
     * @param userName Text to setting.
     */
    public void setUserName(String userName){
        userNameField.sendKeys(userName);
    }

    /**
     * Set specified text into password field.
     *
     * @param password Text to setting.
     */
    public void setPassword(String password){
        passwordField.sendKeys(password);
    }

    public void tapOnForgotPassword(){
        forgotPasswordLink.click();
    }

    public void tapOnRegistration(){
        signInButton.click();
    }

    public void submitForm(){
        logInSubmitButton.click();
    }
}
