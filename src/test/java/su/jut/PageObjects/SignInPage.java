package su.jut.PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import su.jut.TestHelpers.User;

import java.util.List;

public class SignInPage extends CommonPageObject {

    private final WebDriver driver;

    private WebDriverWait wait;

    @FindBy(className = "newsTitle")
    private WebElement signInPageTitle;

    @FindBy(id = "name")
    private WebElement nameField;

    @FindBy(xpath = "/html/body/div[5]/div[1]/div/form/div[2]/table/tbody/tr[2]/td[2]/input[2]")
    private WebElement checkNameButton;

    @FindBy(name = "password1")
    private WebElement passwordField;

    @FindBy(css = "input[name=\"email\"]")
    private WebElement emailField;

    @FindBy(css ="input[name=\"question_answer\"]")
    private WebElement questionField;

    @FindBy(id = "recaptcha-anchor")
    private WebElement capcha;

    @FindBy(className = "fbutton")
    private WebElement submitButton;

    @FindBy(className = "berrors")
    private WebElement signInSuccessRequestTitle;

    public SignInPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @Override
    public void waitScreen() {
        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(signInPageTitle));
    }

    public void setTextInToNameField(String login){
        nameField.sendKeys(login);
    }

    public void setTextInToPasswordField(String password){
        passwordField.sendKeys(password);
    }

    public void setTextInToEmailField(String email){
        emailField.sendKeys(email);
    }

    public void setTextInToQuestionField(String answer){
        questionField.sendKeys(answer);
    }

    public void submitSignInForm(){
        submitButton.click();
    }

    public void fillSignInForm(User user){
        nameField.sendKeys(user.name);
        passwordField.sendKeys(user.password);
        emailField.sendKeys(user.email);
        questionField.sendKeys("Наруто");

        // Handling captcha checkbox (Я не смог отхендлить капчу)
    }

    public void waitSuccessRequestTitle(){
        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(signInSuccessRequestTitle));
    }

    public void waitValidationPopUpWithText(String text) {
        wait = new WebDriverWait(driver, 10);
        WebElement popUp = driver.findElement(By.id("dlepopup"));
        String actualText = popUp.getText();
        wait.until(ExpectedConditions.visibilityOf(popUp));
        Assert.assertEquals(actualText, text, "Wrong text in " + popUp.getTagName() + " tag on " +
                SignInPage.class.getName());
    }

}
