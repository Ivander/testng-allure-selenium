package su.jut.PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Abstract class for common page scheme, which presents in all other pages.
 */
abstract public class CommonPageObject {

        @FindBy(xpath = "/html/body/div[2]/div")
        protected WebElement topNavigationBar;

        @FindBy(className = "info_panel")
        protected WebElement infoPanel;

        public CommonPageObject(WebDriver driver) {
                PageFactory.initElements(driver, this);
        }

        abstract void waitScreen();

}
