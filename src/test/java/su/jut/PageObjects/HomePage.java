package su.jut.PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.function.Function.*;

public class HomePage extends CommonPageObject {

    private static WebDriver driver;

    private WebDriverWait wait;


    // This ia unique element for HomePage. So lets use him for waitScreen method.
    @FindBy(className = "promo_text")
    private WebElement onQueueContainer;

    @FindBy(className = "login_btn")
    private WebElement loginButton;

    public HomePage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @Override
    public void waitScreen() {
        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(onQueueContainer));
    }

    public HomePage goToHomePage(){
        driver.get("https://jut.su/");
        return new HomePage(driver);
    }

    public void openLogInPage(){
        loginButton.click();
    }



}
