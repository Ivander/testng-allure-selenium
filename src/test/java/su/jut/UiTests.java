package su.jut;

import io.qameta.allure.Feature;
import org.testng.Assert;
import org.testng.annotations.Test;
import su.jut.PageObjects.HomePage;
import su.jut.PageObjects.LogInPage;
import su.jut.PageObjects.SignInPage;
import su.jut.TestHelpers.User;


public class UiTests extends WebDriverSettings {

    @Feature("Sign up")
    @Test(groups = "TestToPass")
    public static void verifyRegistrationWithValidParams() {
        HomePage home = new HomePage(driver);
        home.goToHomePage().waitScreen();
        home.openLogInPage();

        LogInPage login = new LogInPage(driver);
        login.waitScreen();
        login.tapOnRegistration();
        SignInPage signIn = new SignInPage(driver);
        signIn.waitScreen();

        User user = new User();
        signIn.fillSignInForm(user);
        signIn.waitSuccessRequestTitle();
    }

    @Feature("Sign up")
    @Test(groups = "TestToFail")
    public static void submitEmptySignInForm() {
        HomePage home = new HomePage(driver);
        home.goToHomePage().waitScreen();
        home.openLogInPage();

        LogInPage login = new LogInPage(driver);
        login.waitScreen();
        login.tapOnRegistration();
        SignInPage signIn = new SignInPage(driver);
        signIn.waitScreen();
        signIn.submitSignInForm();
        signIn.waitValidationPopUpWithText("Имя пользователя не может быть пустым!");
    }

    @Feature("Sign up")
    @Test(groups = "TestToFail")
    public static void verifyPasswordValidationMessage() {
        HomePage home = new HomePage(driver);
        home.goToHomePage().waitScreen();
        home.openLogInPage();

        LogInPage login = new LogInPage(driver);
        login.waitScreen();
        login.tapOnRegistration();
        SignInPage signIn = new SignInPage(driver);
        signIn.waitScreen();
        signIn.setTextInToNameField("Ivan");
        signIn.setTextInToPasswordField("");
        signIn.submitSignInForm();
        signIn.waitValidationPopUpWithText("Длина пароля должна быть не менее 6 символов!");
    }

    @Feature("Sign up")
    @Test(groups = "TestToFail")
    public static void verifyEmailValidationMessage() {
        HomePage home = new HomePage(driver);
        home.goToHomePage().waitScreen();
        home.openLogInPage();

        LogInPage login = new LogInPage(driver);
        login.waitScreen();
        login.tapOnRegistration();
        SignInPage signIn = new SignInPage(driver);
        signIn.waitScreen();
        signIn.setTextInToNameField("Ivan");
        signIn.setTextInToPasswordField("aaaaaaaa");
        signIn.setTextInToEmailField("");
        signIn.submitSignInForm();
        signIn.waitValidationPopUpWithText("Введён неверный e-mail адрес!");
    }

    @Feature("Login")
    @Test(groups = "TestToFail")
    public static void loginWithWrongPassword() {
        HomePage home = new HomePage(driver);
        home.goToHomePage().waitScreen();
        home.openLogInPage();

        LogInPage login = new LogInPage(driver);
        login.waitScreen();
        login.setUserName("ivander");
        login.setPassword("As5888Hyqn111");
        login.submitForm();
        String actualMessage = login.waitAndGetFailedLoginMessage();
        Assert.assertEquals(actualMessage,
                "Ошибка авторизации\n" +
                        "Внимание! Вход на сайт не был произведён. Возможно, Вы ввели неверное имя пользователя или пароль.",
                " Wrong message");
    }

    @Feature("Login")
    @Test(groups = "TestToFail")
    public static void loginWithWrongUserName() {
        HomePage home = new HomePage(driver);
        home.goToHomePage().waitScreen();
        home.openLogInPage();

        LogInPage login = new LogInPage(driver);
        login.waitScreen();
        login.setUserName("ivander111");
        login.setPassword("As5888Hyqn");
        login.submitForm();
        String actualMessage = login.waitAndGetFailedLoginMessage();
        Assert.assertEquals(actualMessage,
                "Ошибка авторизации\n" +
                        "Внимание! Вход на сайт не был произведён. Возможно, Вы ввели неверное имя пользователя или пароль.",
                " Wrong message");
    }

    @Feature("Login")
    @Test(groups = "TestToFail")
    public static void submitEmptyLoginForm() {
        HomePage home = new HomePage(driver);
        home.goToHomePage().waitScreen();
        home.openLogInPage();

        LogInPage login = new LogInPage(driver);
        login.waitScreen();
        login.setUserName("");
        login.setPassword("");
        login.submitForm();
        String actualMessage = login.waitAndGetFailedLoginMessage();
        Assert.assertEquals(actualMessage,
                "Ошибка авторизации\n" +
                        "Внимание! Вход на сайт не был произведён. Возможно, Вы ввели неверное имя пользователя или пароль.",
                " Wrong message");
    }
}

