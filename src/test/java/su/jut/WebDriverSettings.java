package su.jut;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class WebDriverSettings {

    public static WebDriver driver;

    @BeforeClass
    public void onStart() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        Proxy proxy = new Proxy();
        options.addArguments("--proxy-server=http://" + proxy);
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
    }

    @AfterClass
    public void finish() {
        driver.quit();
    }
}
