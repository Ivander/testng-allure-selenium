package su.jut.TestHelpers;

import java.util.HashSet;
import java.util.Random;

/**
 * Here is a helper to generate users with unique (in some range) properties.
 */
public class User {

    public String name;

    public String email;

    public String password;

    public enum Domains {

        GMAIL("@gmail.com"),
        MAILRU("@mail.ru"),
        YAHOO("@yahoo.com"),
        YANDEX("@yandex.ru");

        private String domen;

        Domains(String s) {
            this.domen = s;
        }

        @Override
        public String toString() {
            return "" + this.domen;
        }
    }

    private static HashSet<Integer> allUsersSet = new HashSet<>();

    public User() {
        Random random = new Random();
        int r = random.nextInt(200) + 1;

        if (allUsersSet.contains(r)) {
            r = r * 100;
        }

        this.name = "Ivan" + r;
        switch (r % 5) {
            case (4):
                this.email = "ivan" + r + Domains.GMAIL;
                break;
            case (3):
                this.email = "ivan" + r + Domains.MAILRU;
                break;
            case (2):
                this.email = "ivan" + r + Domains.YAHOO;
                break;
            case (1):
                this.email = "ivan" + r + Domains.YANDEX;
                break;
            default:
                this.email = "someMail" + r / 3 + "@gmail.come";
        }

        this.password = "ivpass" + r;

        allUsersSet.add(r);

        System.out.println(" User was created: " + this);
    }

    @Override
    public String toString() {
        return "Name: " + this.name + ", Email: " + this.email + ", Password: " + this.password;
    }
}
